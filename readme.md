# Comparison

### Forms 

Symfony has a form classes and I think we don't need to use this, because we connect frontend and backend, and if we will use forms and we will need to change form's algoritm for ajax, we will have a problem. T
his is very hard, we will need rebuild all of our code, and we will spend a lot of time.

Laravel does not have form classes, and we don't have this problem )

### Bundles 

Symfony's best practices recommends using only one app bundle, but a lot of people use several bundles, and this is bad. We blurring our logic. A lot of people use several bundles, because this is easy, and it seems logical, but it's not true.

Larave does not have bundles, it has only one app.

### Multifunctional

Symfony understands a lot of config formats: yml, xml, php and ini, and this is bad. This is good for library, but not for frameworks, because more people do not read symfony's best practices and every time each developer choose his favorite format, we have different formats for different projects, and every time we need to get used to different format, and read docs again and again.

laravel uses only one format for all every time. If we need to use some functionality, you need to install custom package, so all laravel projects have similar structure and we don't need to spend time for addiction

#### I finished with symfony minus, and start for laravel pluses.

### Scheduler

Laravel have scheduler class and we a control all our crun task for one place, this is very useful, 
if we need add one task for cron for server, I added this task for scheduler in my local file, added in repository and deploy, it's all.

Symfony does not have this functionality, and we need added cron task for manual every time.

### Queues

Laravel have queues functionality and supported some drivers, database, Beanstalkd, IronMQ, Amazon SQS, Redis.

Symfony does not have this functionality.

### Cache

Laravel have cache classes, but they does not implementation psr-6

Symfony does not have cache

### Filesystem

Laravel using flysystem and we can working on the different cloud storage without problem.

Symfony does not have anything similar
